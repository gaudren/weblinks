package webserver

import (
	"html/template"
	"log"
	"net/http"
	"time"

	"weblinks/database"
)

func StartWebserver() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {

			t, err := template.ParseFiles("webserver/index.gohtml")
			if err != nil {
				panic(err)
			}

			tNow := time.Now()

			data := struct {
				Title     string
				Time      string
				HumanTime string
			}{
				Title:     "Welcome",
				Time:      tNow.Format(time.RFC3339),
				HumanTime: tNow.Format("Mon Jan 2 15:04"),
			}

			err = t.Execute(w, data)
			if err != nil {
				panic(err)
			}
		} else if r.Method == "POST" {
			r.ParseMultipartForm(1000000)
			datas := r.MultipartForm
			for k, headers := range datas.File {
				if k == "uploaded" {
					aux, _ := headers[0].Open()
					fileName := headers[0].Filename
					database.UploadFile(aux, fileName)
				}
			}
		}
	})

	fileServer := http.FileServer(http.Dir("./webserver/static"))
	http.Handle("/static/", http.StripPrefix("/static", fileServer))

	if err := http.ListenAndServe(":8081", nil); err != nil {
		log.Fatal(err)
	}
}
