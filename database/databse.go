package database

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func InitiateMongoClient() *mongo.Client {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	return client
}

func UploadFile(file io.Reader, filename string) uuid.UUID {

	client := InitiateMongoClient()
	bucket := makeBucket(client)

	path := uuid.New()
	uploadOpts := options.GridFSUpload().SetMetadata(bson.D{{"path", path}})

	uploadStream, err := bucket.OpenUploadStream(
		filename,
		uploadOpts,
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer uploadStream.Close()

	fileSize, err := io.Copy(uploadStream, file)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	log.Printf("Write file to DB was successful. File size: %d M\n", fileSize)
	return path
}

func DownloadFile(path uuid.UUID, fileName string) {
	client := InitiateMongoClient()
	bucket := makeBucket(client)

	filter := bson.D{
		{"metadata.path", bson.D{{"$eq", path}}},
	}

	cursor, err := bucket.Find(filter)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	defer func() {
		if err := cursor.Close(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	var foundFiles []gridfs.File
	if err = cursor.All(context.TODO(), &foundFiles); err != nil {
		log.Fatal(err)
	}

	for _, file := range foundFiles {
		var buf bytes.Buffer
		dStream, err := bucket.DownloadToStream(file.ID, &buf)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("File size to download: %v\n", dStream)
		ioutil.WriteFile(fileName, buf.Bytes(), 0600)

		return
	}
}

func Expire() {
	client := InitiateMongoClient()
	bucket := makeBucket(client)

	t := time.Now().Add(-720 * time.Hour)

	filter := bson.D{
		{"uploadDate", bson.D{{"$lt", t}}},
	}

	cursor, err := bucket.Find(filter)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	defer func() {
		if err := cursor.Close(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	var foundFiles []gridfs.File
	if err = cursor.All(context.TODO(), &foundFiles); err != nil {
		log.Fatal(err)
	}

	for _, file := range foundFiles {
		bucket.Delete(file.ID)
	}
}

func makeBucket(client *mongo.Client) *gridfs.Bucket {
	bucket, err := gridfs.NewBucket(
		client.Database("weblinks"),
	)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	return bucket
}
